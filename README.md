# Face Perception

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_face_perception/face_perception.html)

> This is an [AAAMBOS](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/) package. You can find more information about what AAAMBOS is [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/what_is_aaambos) and how to install it [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/).

A face perception for the qibullet sim and naoqi embodiment pkgs.

Provides definition for ImageCapture and FaceEvent. A dummy webcam module for capturing images from your webcam.
