from __future__ import annotations

from enum import Enum, auto
from typing import Type

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgTopicSendingAttribute, MsgPayloadWrapperCallbackReceivingAttribute
from aaambos.std.extensions.module_status.module_status_extension import ModuleStatusInfoComFeatureOut, \
    ModuleStatusExtensionFeature, ModuleStatusExtension
from attrs import define
import cv2

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity

from datetime import datetime, timedelta
from msgspec import Struct, field as m_field
from aaambos.std.communications.utils import create_promise_and_unit, create_in_out_com_feature
from aaambos.std.communications.categories import MSGSPEC_STRUCT_TYPE

IMAGE_CAPTURE = "ImageCapture"


class ImageCaptureStatus(Enum):
    started = auto()
    stopped = auto()
    failed = auto()


class ImageCapture(Struct):
    """ImageCapture datastructure"""
    source: str
    status: ImageCaptureStatus
    location: str
    duration: timedelta
    field_of_view_degrees: tuple[float, float]
    time: datetime = m_field(default_factory=lambda: datetime.now())


ImageCapturePromise, ImageCaptureUnit = create_promise_and_unit(
    name=IMAGE_CAPTURE,
    payload_wrapper=ImageCapture,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

ImageCaptureComFeatureIn, ImageCaptureComFeatureOut = create_in_out_com_feature(
    name=IMAGE_CAPTURE, promise=ImageCapturePromise, version_str="0.1.0", requirements=[],
    version_compatibility_str=">=0.1.0"
)

# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# ImageCaptureComFeatureOut.name: (ImageCaptureComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# ImageCaptureComFeatureIn.name: (ImageCaptureComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# ImageCaptureComFeatureIn.name: (ImageCaptureComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# ImageCaptureComFeatureOut.name: (ImageCaptureComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the ImageCaptureComFeatureOut and ImageCaptureComFeatureIn based on their location.

FACE_EVENT = "FaceEvent"


class FaceEvent(Struct):
    """FaceEvent datastructure"""
    faces_pixel_positions: list[tuple[int, int, int, int]]
    faces_center_distance_percent: list[tuple[float, float, float, float]]
    faces_center_angle: list[tuple[float, float, float, float]]
    time: datetime = m_field(default_factory=lambda: datetime.now())


FaceEventPromise, FaceEventUnit = create_promise_and_unit(
    name=FACE_EVENT,
    payload_wrapper=FaceEvent,
    unit_description="__description of the content of the msg__",
    frequency=1,
    version_str="0.1.0",
    required_service_categories=[{MSGSPEC_STRUCT_TYPE}],
)

FaceEventComFeatureIn, FaceEventComFeatureOut = create_in_out_com_feature(
    name=FACE_EVENT, promise=FaceEventPromise, version_str="0.1.0", requirements=[], version_compatibility_str=">=0.1.0"
)


# # add the features to your modules
# # modules that send the feature (provide out-feature (and require that some module provides the in-feature)):
# # inside the dict of the provides_features method:
# FaceEventComFeatureOut.name: (FaceEventComFeatureOut, SimpleFeatureNecessity.Required),
# # inside the dict of the requires_features method:
# FaceEventComFeatureIn.name: (FaceEventComFeatureIn, SimpleFeatureNecessity.Required)

# # modules that receive the feature (provide in-feature (and require that some module provides the out-feature)):
# # inside the dict of the provides_features method:
# FaceEventComFeatureIn.name: (FaceEventComFeatureIn, SimpleFeatureNecessity.Required)
# # inside the dict of the requires_features method:
# FaceEventComFeatureOut.name: (FaceEventComFeatureOut, SimpleFeatureNecessity.Required),
# # imports: from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
# # and the FaceEventComFeatureOut and FaceEventComFeatureIn based on their location.


class FacePerceptionModule(Module, ModuleInfo):
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService
    config: FacePerceptionModuleConfig
    image_path: str | None = None
    image_duration: timedelta = None
    last_processed: datetime = None
    field_of_view: tuple[int, int] = None

    def __init__(self, config: ModuleConfig, com: CommunicationService, log, ext, status: ModuleStatusExtension, *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.status = status

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            ImageCaptureComFeatureIn.name: (ImageCaptureComFeatureIn, SimpleFeatureNecessity.Required),
            FaceEventComFeatureOut.name: (FaceEventComFeatureOut, SimpleFeatureNecessity.Required),
            FaceEventComFeatureIn.name: (FaceEventComFeatureIn, SimpleFeatureNecessity.Required),
            ModuleStatusInfoComFeatureOut.name: (ModuleStatusInfoComFeatureOut, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            ImageCaptureComFeatureOut.name: (ImageCaptureComFeatureOut, SimpleFeatureNecessity.Required),
            FaceEventComFeatureIn.name: (FaceEventComFeatureIn, SimpleFeatureNecessity.Required),
            FaceEventComFeatureOut.name: (FaceEventComFeatureOut, SimpleFeatureNecessity.Required),
            ModuleStatusExtensionFeature.name: (ModuleStatusExtensionFeature, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return FacePerceptionModuleConfig

    async def handle_image_capture(self, topic, msg: ImageCapture):
        if msg.status == ImageCaptureStatus.started:
            self.image_path = msg.location
            self.image_duration = msg.duration
            self.field_of_view = msg.field_of_view_degrees
            await self.status.set_module_status("start_face_detection", {})
        else:
            self.image_path = None
            await self.status.set_module_status("stopped_face_detection", {})

    async def initialize(self):
        await self.com.register_callback_for_promise(ImageCapturePromise, self.handle_image_capture)

    async def step(self):
        # https://www.datacamp.com/tutorial/face-detection-python-opencv
        if self.image_path and (
                self.last_processed is None or datetime.now() > self.last_processed + self.image_duration):
            img = cv2.imread(self.image_path)
            if img is not None:

                self.last_processed = datetime.now()
                gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                face_classifier = cv2.CascadeClassifier(
                    cv2.data.haarcascades + "haarcascade_frontalface_default.xml"
                )
                face = face_classifier.detectMultiScale(
                    gray_image, scaleFactor=1.1, minNeighbors=5, minSize=(40, 40)
                )
                pixel_positions = []
                center_distance = []
                center_angle = []
                for (x, y, w, h) in face:
                    x,y,w,h = int(x), int(y), int(w), int(h)
                    # somehow the other way around with the img shapes
                    center = int(img.shape[1]) // 2, int(img.shape[0]) // 2

                    pixel_positions.append((x, y, x + w, y + h))
                    center_distance.append(((x - center[0]) / center[0], (y - center[1]) / center[1],
                                            ((x + w) - center[0]) / center[0], ((y + h) - center[1]) / center[1]))
                    center_angle.append((center_distance[-1][0] * self.field_of_view[0],
                                         center_distance[-1][1] * self.field_of_view[1],
                                         center_distance[-1][2] * self.field_of_view[0],
                                         center_distance[-1][3] * self.field_of_view[1]))
                if pixel_positions:
                    face_event = FaceEvent(
                        faces_pixel_positions=pixel_positions,
                        faces_center_angle=center_distance,
                        faces_center_distance_percent=center_angle,
                    )
                    self.log.info(f"Detected face: {pixel_positions}; {img.shape}")
                    await self.com.send(FaceEventPromise.settings.topic, face_event)
                else:
                    self.log.trace("No faces detected.")

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class FacePerceptionModuleConfig(ModuleConfig):
    module_path: str = "face_perception.modules.face_perception"
    module_info: Type[ModuleInfo] = FacePerceptionModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0


def provide_module():
    return FacePerceptionModule
