from __future__ import annotations

import os
import sys
from datetime import timedelta
from typing import Type

import cv2
from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity

from face_perception.modules.face_perception import ImageCapture, ImageCaptureStatus, ImageCaptureComFeatureIn, \
    ImageCaptureComFeatureOut, ImageCapturePromise


class WebcamModule(Module, ModuleInfo):
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService
    config: WebcamModuleConfig
    video_capture: cv2.VideoCapture = None
    exchange_dir: str = None

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {ImageCaptureComFeatureOut.name: (ImageCaptureComFeatureOut, SimpleFeatureNecessity.Required)}

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {ImageCaptureComFeatureIn.name: (ImageCaptureComFeatureIn, SimpleFeatureNecessity.Required)}

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return WebcamModuleConfig

    async def initialize(self):
        self.video_capture = cv2.VideoCapture(self.config.camera_index)
        self.exchange_dir = str(self.config.general_run_config.get_instance_dir() / self.config.exchange_dir)
        if not os.path.exists(self.exchange_dir):
            os.makedirs(self.exchange_dir)

    async def step(self):
        result, video_frame = self.video_capture.read()
        if result is False:
            ImageCapture(
                source="webcam",
                status=ImageCaptureStatus.failed,
                location="",
                duration=timedelta(seconds=2),
                field_of_view_degrees=(90.0,90.0),
            )
            sys.exit(1)
        loc = self.exchange_dir + "/WebcamImg.png"
        cv2.imwrite(loc, video_frame)
        image_capture = ImageCapture(
            source="webcam",
            status=ImageCaptureStatus.started,
            location=loc,
            duration=timedelta(seconds=2),
            field_of_view_degrees=(90.0,90.0),
        )
        await self.com.send(ImageCapturePromise.settings.topic, image_capture)

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class WebcamModuleConfig(ModuleConfig):
    module_path: str = "face_perception.modules.webcam_module"
    module_info: Type[ModuleInfo] = WebcamModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    camera_index: int = 0
    exchange_dir: str = "webcam_exchange"


def provide_module():
    return WebcamModule
