"""

This is the documentation of the aaambos package Face Perception.
It contains of aaambos modules, owm run and/or arch configs,

# About the package
It implements a simple face perception module that uses opencv haarcascade classifier for face detection.

It is based on this [online tutoria](https://www.datacamp.com/tutorial/face-detection-python-opencv).

# Background / Literature
There it is a lot of research about face perception.

Future versions might include more complex perception, like better face perception, a face recognition, face attribute detection and/or emotion detection.

# Usage / Examples
The package contains a FacePerceptionModule and a dummy webcamModule for webcam input

It defines the `ImageCapture` and `FaceEvent` promises.

The ImageCapture informs about the started capturing process of images. It should be sent from another module, e.g., webcam module (dummy), NaoQi Embodiment, QiBullet Sim.

The FaceEvent informs about detected faces.

```python
class FaceEvent(Struct):
    faces_pixel_positions: list[tuple[int, int, int, int]]
    faces_center_distance_percent: list[tuple[int, int, int, int]]
    faces_center_angle: list[tuple[int, int, int, int]]
    time: datetime = m_field(default_factory=lambda: datetime.now())

class ImageCapture(Struct):
    source: str
    status: ImageCaptureStatus
    location: str
    duration: timedelta
    field_of_view_degrees: tuple[int, int]
    time: datetime = m_field(default_factory=lambda: datetime.now())
```

See arch_config.yml for an example usage inside a config.

# Citation


"""
